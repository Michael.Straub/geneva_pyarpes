# -*- coding: utf-8 -*-
"""
Created on Mon Dec 19 15:18:26 2022

@author: straubm

Default configuration file for arpes unige
"""

__all__ = ['default_location']

print('default settings set for university of geneva')

default_location = 'unige'
