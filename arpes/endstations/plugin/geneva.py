# -*- coding: utf-8 -*-
"""
Created on Mon Dec  5 15:31:56 2022

@author: straubm

krx loader for the 64 bit system of MBS A1Soft LV 2016 Ver.16.0f5
"""

import xarray as xr
import numpy as np
import os
import warnings
import struct
from typing import List
import itertools
from arpes.endstations import EndstationBase


__all__ = ['krx_data']

def load_krx(file_path:str):
    """load wrapper for the krx data format 64 bit (MBS system GENEVA)"""
    
    krx_data_loader = krx_data()
    
    data = krx_data_loader.load(file_path)
    
    return data

def update_dict(dictionary: dict, entry):
    """
    handy function to create new dictionary with updated values
    """
    
    new_dictionary = dictionary.copy()
    
    new_dictionary.update(entry)
    
    return new_dictionary

class krx_data(EndstationBase):
    """
    krx data loader class
    """
    
    PRINCIPAL_NAME = "krx-loader"
    ALIASES = ['krx','unige']
    
    _TOLERATED_EXTENSIONS = ['.krx']
    
    CONCAT_COORDS = ["x", "y","z",'mapX','mapY']
    
    RENAME_KEYS = {'Kinetic Energy(eV)' : 'kinE',
                   'Angle(Degrees)' : 'polar'}
    
    
    @classmethod
    def load(self, file_path: dict = None,trace = None,**kwargs):
        """
        The actual load function to load the data works as follows:
         - resolve_frame_location autocompletes and resolves the frame location.
         - load_meta_data Load the meta data like image_size and position in the file
         - load_header finds and loads the header of the first image into a dictionary
             if data is a scan it creates a header for each image
         - create_coords takes the header and creates coordinates 2d coordinates
         - load_single_image in a big list
         - make xarrays with the coordinates and headers (as attributes)
         - concate the list along changing attributes
         - quick postprocessing (changing coordinate names and delete singular dimensions)        

        Parameters
        ----------
        path : TYPE str
            DESCRIPTION: 
            relative or direct path to the file. (will try to autocomplete filename)
        Returns
        -------
        xarray containing the data

        """
        
        resolved_location = self.resolve_frame_location(self,file_path)
        
        meta_data_list = self.load_meta_data(self,resolved_location)
        
        header_list = self.load_header(self,resolved_location, meta_data_list[0])
        
        coords = self.create_coords(self,header_list[0], meta_data_list)

        image_list = [self.load_single_image(self,resolved_location, meta_data) for meta_data in meta_data_list]

        data_list = [xr.DataArray(image, coords = coords, attrs = header, dims = ['Kinetic Energy(eV)','Angle(Degrees)'])
                     for image, header in zip(image_list, header_list)]
        
        concated = self.concatenate_data(self, data_list, **kwargs)
        
        processed = self.postprocess(self, concated)
        
        
        return processed
        
    
    def resolve_frame_location(self, scan_desc: str) -> list:
        """
        tries to find the single file
        scan description should give a path to single file according to: path/file*
        If the file is a krx file: It returns a list of meta data of the individual images in the data set
        If the file/s are txt files: It returns a list of file locations with the prefix according to scan_desc 
        """
        if scan_desc is None:
            raise ValueError(
                "Must pass a scan description to load data with this endstation loading code."
            )
        #if the function gets used from the pyarpes lib
        elif type(scan_desc) is dict:
            file = scan_desc['file']
        #if the function gets used directly
        elif type(scan_desc) is str:
            file = scan_desc
        else:
            raise NameError(
                "Cannot interprete the scan description as file")
        
        dir_path = os.path.dirname(file)
        file_prefix = os.path.basename(file)
        data_list = []
        
        #find the file and check the suffix
        for file in os.listdir(dir_path):
            if file.startswith(file_prefix):
                if file.endswith('.krx'):
                    data_list.append(os.path.join(dir_path, file))
        #at the moment loading multiple files at once is not implemented
        if len(data_list)> 1:
            raise NameError("More than one file using this prefix has been found")
        if len(data_list) == 0:
            raise NameError("No file uisng with this name or starting with it has been found")
        
        resolved_location = data_list[0]
        
        return resolved_location
    
    def load_meta_data(self, path_to_file: str) -> list:
        """
        This method looks for the image position and size in the
        topheader of the file and returns a list with the information
        needed to load the individual images.
        """
        
        with open(path_to_file, 'rb') as file:
            v0 = struct.unpack('=q', file.read(8)) #meta information is in 64bit int
            n_images = int(v0[0]/3)# I have no idea why the number is multiplied by three
            # this is just copied from the igor algorithm. (C)Felix
            
            meta_data_images = []
            
            for i in range(n_images):
                # While the data is 32bit, the meta data (for some reason) is 64 bit
                image_pos = struct.unpack('=q', file.read(8))[0]
                image_sizeY = struct.unpack('=q', file.read(8))[0]
                image_sizeX = struct.unpack('=q', file.read(8))[0]
                
                image_meta = (image_pos, image_sizeY, image_sizeX, i)
                
                meta_data_images.append(image_meta)
        return meta_data_images
    
    def load_header(self, path_to_file: str, image_meta: tuple, attr_dict_list = None) -> dict:
        """
        locates the header in the file and creates a dictionary 
        with all the information in it. Creates dictionary with additional attributes according to header
        Additional infos about the header:
            - each image has its own header behind the actual image
            - each image has the same header (only map coordinate might/or not change)
            - loading the header seems to be problematic sometimes for the last image.
              Therefore only the first header is loaded.
        
        If multiple images are in the file, the header gets copied for each image and a list of attribute 
        dictionaries is returned. In this case also some information is tried to be resolved to add 
        missing scanning coordinates according to first header.
        Implemented scans are:
            - 2d spatial scans
            - Scan in mapX direction
        """
        image_pos = image_meta[0]
        image_sizeY = image_meta[1]
        image_sizeX = image_meta[2]
        image_number = image_meta[3]
        
        #for some reason the 'header' is after the image (more like a footnote)
        header_pos = (image_pos + image_sizeX * image_sizeY + 1) * 4
        attr_dict = {}
        with open(path_to_file) as f:
            f.seek(header_pos)
            while True:
                line = f.readline().split('\t')
                key = line[0]
                val = line[1][0:-1] #[0:-1] gets rid of '\n'
        
                if key == 'DATA:': break
                try:
                    attr_dict[key] = float(val)
                except ValueError:
                    attr_dict[key] = val
        
        for key in attr_dict.keys():
            if key.startswith('2D scan'):
                split_line = key.split()
                
                scan0_name = split_line[2]
                scan0_start = float(split_line[3])
                scan0_stop = float(split_line[4])
                scan0_step =float(split_line[5])
                scan0_num = round((scan0_stop-scan0_start)/scan0_step +1)
                scan0_range = np.linspace(scan0_start, scan0_stop, scan0_num)
                
                scan1_name = split_line[6]
                scan1_start = float(split_line[7])
                scan1_stop = float(split_line[8])
                scan1_step =float(split_line[9])
                scan1_num = round((scan1_stop-scan1_start)/scan1_step +1)
                scan1_range = np.linspace(scan1_start, scan1_stop, scan1_num)

                
                S1,S0 = np.meshgrid(scan1_range, scan0_range)
                
                scan0_coord = S0.flatten()
                scan1_coord = S1.flatten()
                
                attr_dict_list = [update_dict(attr_dict, { scan0_name: scan0_var, scan1_name: scan1_var })
                              for scan0_var, scan1_var in zip(scan0_coord, scan1_coord)]
            
            if key.startswith('MapNoXSteps'):
                scanX_name = 'mapX'
                scanX_range = np.linspace(attr_dict['MapStartX'], attr_dict['MapEndX'], int(attr_dict['MapNoXSteps']))
                scanY_name = 'mapY'
                scanY_range = np.linspace(attr_dict['MapStartY'], attr_dict['MapEndY'], int(attr_dict['MapNoYSteps']))
                
                SX, SY = np.meshgrid(scanX_range, scanY_range)
                
                scanX_coord = SX.flatten()
                scanY_coord = SY.flatten()

                attr_dict_list = [update_dict(attr_dict, { scanX_name: scanX_var, scanY_name: scanY_var })
                              for scanX_var, scanY_var in zip(scanX_coord, scanY_coord)]
                
        if attr_dict_list is None:
            attr_dict_list = [attr_dict]
        
        return attr_dict_list
    
    def create_coords(self, header, meta_data_list):
        """
        Creates the coordinate for each of the images by looking in the header for the start and stops of each dimension
        """
        
        kin_name = 'Kinetic Energy(eV)'    
        kin_start = header['Start K.E.']
        kin_stop = header['End K.E.']
        image_sizeX = meta_data_list[0][2]
        kin_coord = np.linspace(kin_start, kin_stop, image_sizeX)

        scale_name = header['ScaleName']
        scale_start = header['ScaleMin']
        scale_stop = header['ScaleMax']
        image_sizeY = meta_data_list[0][1]
        scale_coord = np.linspace(scale_start, scale_stop , image_sizeY)

        coords = {kin_name: kin_coord, scale_name: scale_coord}
        
        return coords
    
    def load_single_image(self,resolved_location, meta_data):
        """
        find and load the data in a krx file
        """
        image_pos = meta_data[0]
        image_sizeY = meta_data[1]
        image_sizeX = meta_data[2]

        data_length = image_sizeY * image_sizeX

        # image is a image_sizeY * image_sizeX long data array 
        # using 32 bit (4byte) unsigned integer
        image = np.fromfile(resolved_location, dtype= np.int32,
                            count = data_length, offset = image_pos*4)
        
        loaded_image = np.reshape(image, (image_sizeY, image_sizeX)).T
        
        return loaded_image
    
    def concatenate_data(self, frames=List[xr.Dataset], reverse = False):
        """Performs concatenation of frames in multi-image scans. (copied from pyarpes package by chstan)

        The way this happens is that we look for an axis on which the frames are changing uniformly
        among a set of candidates (`.CONCAT_COORDS`). Then we delegate to xarray to perform the concatenation
        and clean up the merged coordinate.
        """
        if not frames:
            raise ValueError("Could not read any frames.")

        if len(frames) == 1:
            return frames[0]

        # determine which axis to stitch them together along, and then do this
        scan_coord = []
        shape = []
        max_different_values = 1
        for possible_scan_coord in self.CONCAT_COORDS:
            coordinates = [f.attrs.get(possible_scan_coord, None) for f in frames]
            n_different_values = len(set(coordinates))
            if n_different_values > max_different_values and None not in coordinates:
                shape.append(n_different_values)
                scan_coord.append(possible_scan_coord)
        
        for new_coord in scan_coord:
            for f in frames:
                f.coords[new_coord] = f.attrs[new_coord]
        
        if len(scan_coord) > 2:
            raise ValueError("More than two external scan dimensions found. TODO implement")
        
        if len(scan_coord) ==1:
            #1dmaps
            frames.sort(key = lambda x: x.attrs[scan_coord[0]], reverse = reverse)
            return xr.concat(frames, dim = scan_coord[0])
            
        if len(scan_coord) ==2 :
            #2dmaps
            frames.sort(key = lambda x: x.attrs[scan_coord[1]], reverse = reverse)
            frames.sort(key = lambda x: x.attrs[scan_coord[0]], reverse = reverse)
            grouped_data = [list(group) for key,group in itertools.groupby(frames, key = lambda x: x.attrs[scan_coord[0]])]
            
            concatted_list = [xr.concat(sub_list,scan_coord[1]) for sub_list in grouped_data]
            
            return xr.concat(concatted_list, scan_coord[0])
            
        if len(scan_coord) == 0:
            raise NameError(f'Scanning coordinate could not be found in CONCAT_COORDS : {self.CONCAT_COORDS}')
    
    def postprocess(self, frames: xr.Dataset):
        """Performs frame level normalization of scan data.

        Here, we currently:
        1. Remove dimensions if they only have a single point, i.e. if the scan has shape [1,N] it
          gets squeezed to have size [N]
        2. Rename coordinates
        """
        
        frames = frames.rename(self.RENAME_KEYS)

        sum_dims = []
        for dim in frames.dims:
            if len(frames.coords[dim]) == 1 and dim in self.SUMMABLE_NULL_DIMS:
                sum_dims.append(dim)

        if sum_dims:
            frames = frames.sum(sum_dims, keep_attrs=True)

        return frames