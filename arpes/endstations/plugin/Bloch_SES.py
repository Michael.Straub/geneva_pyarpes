"""
Implements data loading for the SES data at Bloch MaxLAB IV

Much of the code is from Craig Polley (beamline scientist at Bloch)
https://gitlab.com/flashingLEDs/pesto
"""


import xarray as xr
import numpy as np
from igor import binarywave
import zipfile
import linecache
import copy
import os
import warnings
import struct
from typing import List
import itertools
from arpes.endstations import EndstationBase


__all__ = ['SES_data']

class SES_data(EndstationBase):
    """
    loader class for the SES_data at Bloch
    """
    PRINCIPAL_NAME = "BLOCH"
    ALIASES = ['SES','bloch','maxiv']

    _TOLERATED_EXTENSIONS = ['.txt','.ibw','.zip','.itx','sp2']

    #CONCAT_COORDS = ["x", "y", "z", 'mapX', 'mapY']

    RENAME_KEYS = {'Kinetic energy': 'kinE',
                   'Angle': 'polar',
                   'X':'x',
                   'Y':'y',
                   'Z':'z'}


    def load(self, scan_desc: dict = None, **kwargs) -> xr.Dataset:
        """load wrapper for the SES data at Bloch

        The idea is to use the function from Craig to load the data and then reconfigure them
        to be useful for the pyarpes procedures
        """
        #DataLoader from Craig
        scan = loadSpectrum(scan_desc['file'], beQuiet= True)
        #create coordinate dictionary
        coords = {AxisLabel : Axis for AxisLabel, Axis in zip(scan['AxisLabel'],scan['Axis'] )}
        #create Dataset
        dataset = xr.DataArray(scan['data'] , coords = coords, attrs = scan['Metadata'], dims = scan['AxisLabel'])
        #postprocess deletes empty dimensions and Renames Keys
        processed = self.postprocess(dataset)

        return processed

    def postprocess(self, frames: xr.Dataset):
        """Performs frame level normalization of scan data.

        Here, we currently:
        1. Remove dimensions if they only have a single point, i.e. if the scan has shape [1,N] it
          gets squeezed to have size [N]
        2. Rename coordinates
        """

        for prev_name, new_name in self.RENAME_KEYS.items():
            try:
                frames = frames.rename({prev_name : new_name})
            except ValueError:
                frames = frames.assign_attrs({new_name: None})

        sum_dims = []
        for dim in frames.dims:
            if len(frames.coords[dim]) == 1 and dim in self.SUMMABLE_NULL_DIMS:
                sum_dims.append(dim)

        if sum_dims:
            frames = frames.sum(sum_dims, keep_attrs=True)

        return frames

#######################################################################################################################
######################################### Work of Craig Polley ########################################################
#######################################################################################################################


def loadSpectrum(fileName, beQuiet=False, regionIndex=1, whichManipulatorAxis=''):
    assert (fileName.endswith(".txt")
            or fileName.endswith(".ibw")
            or fileName.endswith(".zip")
            or fileName.endswith(".itx")
            or fileName.endswith(
                ".sp2")), "Only SES produced .txt,.ibw and .zip files and Prodigy produced SP2 files are currently supported"

    def load_sp2(fileName, beQuiet=False, regionIndex=1):
        loadedSpectrum = {}
        loadedSpectrum['Metadata'] = {}
        loadedSpectrum['Axis'] = [[], []]
        loadedSpectrum['AxisLabel'] = ["", ""]
        loadedSpectrum['AxisUnits'] = ["", ""]
        with open(fileName) as fp:
            dataStreamStartLine = 999999

            for i, line in enumerate(fp):
                if i < dataStreamStartLine:
                    if line.startswith("# lensmode"): loadedSpectrum['Metadata']['Lens Mode'] = line.replace(
                        "# lensmode           = \"", "").rstrip('"\n')
                    if line.startswith("# Ep"): loadedSpectrum['Metadata']['Pass Energy'] = float(
                        line.replace("# Ep                 = ", "").replace("# [eV]\n", ""))

                    if line.startswith("# Creation Date"):
                        values = line.replace("# Creation Date = \"", "").split(" ")
                        loadedSpectrum['Metadata']['Date'] = values[0]
                        loadedSpectrum['Metadata']['Time'] = values[1]

                    if line.startswith("# ERange"):
                        values = line.replace("# ERange             = ", "").split(" ")
                        energyStart = float(values[0])
                        energyEnd = float(values[1])

                    if line.startswith("# aRange"):
                        values = line.replace("# aRange             = ", "").split(" ")
                        angleStart = float(values[0])
                        angleEnd = float(values[1])

                    if line.startswith("# SIZE_X"): numEnergySteps = int(
                        line.replace("# SIZE_X                  = ", "").replace(" # [pixel]", ""))

                    if line.startswith("# SIZE_Y"): numAngleSteps = int(
                        line.replace("# SIZE_Y                  = ", "").replace(" # [pixel]", ""))
                    try:
                        if float(line.split(" ")[0]) == numEnergySteps and float(line.split(" ")[1]) == numAngleSteps:
                            dataStreamStartLine = i + 1
                            loadedImage = np.zeros(numAngleSteps * numEnergySteps)

                    except ValueError:
                        pass

                if i >= dataStreamStartLine:
                    loadedImage[i - dataStreamStartLine] = float(line)

        loadedSpectrum['data'] = loadedImage.reshape((numAngleSteps, numEnergySteps,)).T
        loadedSpectrum['Axis'][1] = np.linspace(angleStart, angleEnd, numAngleSteps)
        loadedSpectrum['AxisLabel'][1] = "Angle"
        loadedSpectrum['AxisUnits'][1] = "$\degree$"
        loadedSpectrum['Axis'][0] = np.linspace(energyStart, energyEnd, numEnergySteps)
        loadedSpectrum['AxisLabel'][0] = "Kinetic energy"
        loadedSpectrum['AxisUnits'][0] = "eV"

        loadedSpectrum['Metadata']['Acquisition Mode'] = 'Fixed'
        loadedSpectrum['Metadata']['File Path'] = fileName
        return loadedSpectrum

    def load_itx(fileName):

        loadedSpectrum = {}
        loadedSpectrum['Metadata'] = {}

        file = open(fileName).readlines()

        label, units, axisStart, axisEnd = [], [], [], []
        for i, line in enumerate(file):
            if 'WAVES/S/N' in line:
                dimensions = [int(ii) for ii in line.split("=")[1].split(" ")[0][1:-1].split(
                    ",")]  # Example: WAVES/S/N=(500,500) '1_Spectrum'
            if '//Created Date' in line:
                loadedSpectrum['Metadata']['Date'] = line.split(": ")[1].split(" ")[0]
                loadedSpectrum['Metadata']['Time'] = line.split(": ")[1].split(" ")[1]
            # print(line)

            if '//Scan Mode' in line:        loadedSpectrum['Scan Mode']['Number of Sweeps'] = int(
                line.split("=")[1].rstrip("\n"))
            if '//User Comment' in line:        loadedSpectrum['Metadata']['Comments'] = line.split("=")[1].rstrip("\n")
            if '//Analysis Mode' in line:    loadedSpectrum['Analysis Mode']['Number of Sweeps'] = int(
                line.split("=")[1].rstrip("\n"))
            if '//Lens Mode' in line:        loadedSpectrum['Metadata']['Lens Mode'] = line.split("= ")[1].rstrip("\n")
            if '//Lens Voltage' in line:        loadedSpectrum['Metadata']['Lens Voltage'] = line.split("= ")[1].rstrip(
                "\n")
            if '//Analyzer Slits' in line:    loadedSpectrum['Metadata']['Analyzer Slits'] = line.split("= ")[1].rstrip(
                "\n")
            if '//Number of Scans' in line:    loadedSpectrum['Metadata']['Number of Sweeps'] = int(
                line.split("=")[1].rstrip("\n"))
            # if '//Number of Samples' in line: print(line.split("=")[1].rstrip("\n"))
            # if '//Scan Step' in line: 		loadedSpectrum['Metadata']['Energy Step']=float(line.split("=")[1].rstrip("\n"))
            # if '//DwellTime' in line: 		print(line.split("=")[1].rstrip("\n"))
            # if '//Excitation Energy' in line: print(line.split("=")[1].rstrip("\n"))
            # if '//Kinetic Energy' in line: 	print(line.split("=")[1].rstrip("\n"))
            # if '//Binding Energy' in line: 	print(line.split("=")[1].rstrip("\n"))
            if '//Pass Energy' in line:        loadedSpectrum['Metadata']['Pass Energy'] = float(
                line.split("=")[1].rstrip("\n"))
            # if '//Retarding Ratio' in line: 	print(line.split("=")[1].rstrip("\n"))
            # if '//Bias Voltage' in line: 		print(line.split("=")[1].rstrip("\n"))
            # if '//Detector Voltage' in line: 	print(line.split("=")[1].rstrip("\n"))
            if '//WorkFunction' in line:        loadedSpectrum['Metadata']['WorkFunction'] = float(
                line.split("=")[1].rstrip("\n"))

            if 'BEGIN\n' in line:    headerLength = i

            if any(x in line for x in ['SetScale/I x', 'SetScale/I y', 'SetScale/I z']):
                #
                info = line.split(", ")

                label.append(info[3].split("(")[1].lstrip(" (").rstrip(")\""))
                units.append(info[3].split("(")[0].rstrip(" (").lstrip("\""))
                axisStart.append(float(info[1]))
                axisEnd.append(float(info[2]))

        if len(dimensions) == 2:
            loadedSpectrum['Axis'] = [[], []]
            loadedSpectrum['AxisLabel'] = ["", ""]
            loadedSpectrum['AxisUnits'] = ["", ""]

            loadedSpectrum['data'] = np.zeros((dimensions[0], dimensions[1]), np.float32)

            for ii in range(dimensions[0]):
                loadedSpectrum['data'][ii, :] = [float(ii) for ii in
                                                 file[headerLength + 1 + (ii)].rstrip("\n").split(" ")]

        if len(dimensions) == 3:
            loadedSpectrum['Axis'] = [[], [], []]
            loadedSpectrum['AxisLabel'] = ["", "", ""]
            loadedSpectrum['AxisUnits'] = ["", "", ""]

            loadedSpectrum['data'] = np.zeros((dimensions[0], dimensions[1], dimensions[2]), np.float32)

            for frameIndex in range(dimensions[2]):
                for energyIndex in range(dimensions[1]):
                    loadedSpectrum['data'][:, energyIndex, frameIndex] = np.fromstring(
                        file[headerLength + 1 + (frameIndex) * (dimensions[0] + 1) + energyIndex], dtype=float, sep=' ')
            loadedSpectrum['data'] = loadedSpectrum['data'].transpose(1, 0, 2)

        for axisIndex in range(len(dimensions)):

            if label[axisIndex] == 'Non-Energy Channel':
                label[axisIndex] = "Analyzer angle"
            if label[axisIndex] == 'SAL X': label[axisIndex] = "Deflector angle"
            loadedSpectrum['AxisLabel'][axisIndex] = label[axisIndex]
            loadedSpectrum['AxisUnits'][axisIndex] = units[axisIndex]
            loadedSpectrum['Axis'][axisIndex] = np.linspace(axisStart[axisIndex], axisEnd[axisIndex],
                                                            num=dimensions[axisIndex], endpoint=True)

        if len(dimensions) == 2:
            return transposeSpectrum(loadedSpectrum, [1, 0])

        if len(dimensions) == 3:
            return transposeSpectrum(loadedSpectrum, [1, 0, 2])

    def load_txt(fileName, beQuiet=False, regionIndex=1, whichManipulatorAxis=''):

        def listRegions(
                fileName):  # Evaluate how many regions are in the file, and where the requested region starts in the file
            regionCounter = 0
            if fileName.endswith(".txt"):
                with open(fileName) as fp:
                    validRegions = []
                    for i, line in enumerate(fp):
                        if line.startswith("[Region "):
                            regionCounter += 1
                            validRegions.append([regionCounter, i])
            return validRegions

        def get_txt_manipulatorscan_axis(fileName, whichManipulatorAxis='', fixRounding=True, regionIndex=1,
                                         beQuiet=False):

            def deleteFromLeft(string, substring):
                if string.startswith(substring):
                    string = string[len(substring):]
                return string

            assert fileName.endswith(".txt"), "Only SES .txt files are supported"

            validRegions = listRegions(fileName)

            with open(fileName) as f:

                energyAxis = []
                analyzerAxis = []
                manipulatorAxisNames = []
                manipulatorAxes = []

                manipulatorInfoStartLine = np.NaN
                eK_axis_line = np.NaN
                manipulatorAxisLength = 0
                for index, line in enumerate(f):
                    if index > validRegions[regionIndex - 1][1]:
                        if line.startswith('Dimension 1 scale='):
                            line = deleteFromLeft(line, 'Dimension 1 scale=')
                            energyAxis = np.fromstring(line.rstrip('\n'), sep='  ').tolist()

                        if line.startswith('Dimension 2 scale='):
                            line = deleteFromLeft(line, 'Dimension 2 scale=')
                            analyzerAxis = np.fromstring(line.rstrip('\n'), sep='  ').tolist()

                        if line.startswith('Dimension 3 size='):
                            line = deleteFromLeft(line, 'Dimension 3 size=')
                            manipulatorAxisLength = int(line.rstrip('\n'))

                        if line.startswith("[Run Mode Information 1]"):
                            manipulatorInfoStartLine = index + 1

                        if index == manipulatorInfoStartLine:
                            for axisName in line.split('\x0b')[1:]:
                                manipulatorAxisNames.append(axisName.rstrip('\n'))
                                manipulatorAxes.append([])

                            if beQuiet == False: print("I found the following axis names in the metadata:",
                                                       [ii for ii in manipulatorAxisNames])

                        if index > manipulatorInfoStartLine and index <= manipulatorInfoStartLine + manipulatorAxisLength:
                            data = line.split('\x0b')[1:]
                            for index, axisName in enumerate(manipulatorAxisNames):
                                manipulatorAxes[index].append(float(data[index]))

                        if index == manipulatorInfoStartLine + manipulatorAxisLength + 1:
                            manipulatorScannedAxisNames = []
                            numberOfScannedAxes = 0
                            for index, axisName in enumerate(manipulatorAxisNames):
                                if manipulatorAxes[index][-1] != manipulatorAxes[index][0]:
                                    numberOfScannedAxes += 1
                                    manipulatorScannedAxisNames.append(axisName)
                                    stepSize = abs(manipulatorAxes[index][-1] - manipulatorAxes[index][0]) / (
                                                manipulatorAxisLength - 1)
                                    if beQuiet == False: print(
                                        "Axis \'{}\' was scanned from {} to {} in steps of {:.5f}".format(axisName,
                                                                                                          manipulatorAxes[
                                                                                                              index][0],
                                                                                                          manipulatorAxes[
                                                                                                              index][
                                                                                                              -1],
                                                                                                          stepSize))

                            if numberOfScannedAxes == 1:
                                # print("\nInferring that the primary manipulator scan axis must be:",[ii for ii in manipulatorScannedAxisNames])
                                primaryAxisIndex = manipulatorAxisNames.index(manipulatorScannedAxisNames[0])
                                primaryManipulatorAxis = manipulatorAxes[primaryAxisIndex]
                                primaryManipulatorAxisName = manipulatorAxisNames[primaryAxisIndex]

                            else:
                                if (whichManipulatorAxis == ''):
                                    if beQuiet == False: print("\n-------- !! WARNING !! ---------")
                                    if beQuiet == False: print(
                                        "I don't know which of these is the primary manipulator scan axis, and you didn't specify.")
                                    if beQuiet == False: print("I will therefore assume that the primary axis is ",
                                                               manipulatorScannedAxisNames[0])
                                    if beQuiet == False: print(
                                        "To choose a different primary axis, please call this function again, passing the additional argument whichManipulatorAxis=n, where n is one of:",
                                        [ii for ii in manipulatorScannedAxisNames])
                                    if beQuiet == False: print("---------------------------------")
                                    primaryAxisIndex = manipulatorAxisNames.index(manipulatorScannedAxisNames[0])
                                    primaryManipulatorAxis = manipulatorAxes[primaryAxisIndex]
                                    primaryManipulatorAxisName = manipulatorAxisNames[primaryAxisIndex]

                                elif whichManipulatorAxis in manipulatorScannedAxisNames:
                                    if beQuiet == False: print(
                                        "Treating {} as the primary scan axis".format(whichManipulatorAxis))
                                    primaryAxisIndex = manipulatorAxisNames.index(whichManipulatorAxis)
                                    primaryManipulatorAxis = manipulatorAxes[primaryAxisIndex]
                                    primaryManipulatorAxisName = manipulatorAxisNames[primaryAxisIndex]

                                else:
                                    if beQuiet == False: print("\n-------- !! WARNING !! ---------")
                                    if beQuiet == False: print(
                                        "The name of the primary manipulator scan axis that you provided ({}) is not the name of a scanned axis ({}).".format(
                                            whichManipulatorAxis, manipulatorScannedAxisNames))
                                    if beQuiet == False: print("I will therefore assume that the primary axis is ",
                                                               manipulatorScannedAxisNames[0])
                                    if beQuiet == False: print(
                                        "To choose a different primary axis, please call this function again, passing the additional argument whichManipulatorAxis=n, where n is one of:",
                                        [ii for ii in manipulatorScannedAxisNames])
                                    if beQuiet == False: print("---------------------------------")
                                    primaryAxisIndex = manipulatorAxisNames.index(manipulatorScannedAxisNames[0])
                                    primaryManipulatorAxis = manipulatorAxes[primaryAxisIndex]
                                    primaryManipulatorAxisName = manipulatorAxisNames[primaryAxisIndex]

                            if fixRounding == True:
                                if beQuiet == False: print(
                                    "I recalculated the axis values, since SES rounds in the file header to 3 decimal places")
                                axisStepSize = (manipulatorAxes[primaryAxisIndex][-1] -
                                                manipulatorAxes[primaryAxisIndex][0]) / (manipulatorAxisLength - 1)
                                axisStart = manipulatorAxes[primaryAxisIndex][0]
                                primaryManipulatorAxis = [axisStart + ii * axisStepSize for ii in
                                                          range(manipulatorAxisLength)]

                            if primaryManipulatorAxisName == 'P':
                                primaryManipulatorAxisName = "Polar angle"
                                axisUnits = "$\degree$"
                            return energyAxis, analyzerAxis, primaryManipulatorAxis, primaryManipulatorAxisName, axisUnits

            assert (False), "Error getting axis information from file"

        # Evaluate how many regions are in the file, and where the requested region starts in the file
        # Avoid enumerating through the entire file if possible, it'll take a while for 3d datasets
        loadedSpectrum = {}
        loadedSpectrum['Metadata'] = {}
        loadedSpectrum['Axis'] = [[], []]
        loadedSpectrum['AxisLabel'] = ["", ""]
        loadedSpectrum['AxisUnits'] = ["", ""]

        validRegions = listRegions(fileName)
        validRegionNumbers = [ii[0] for ii in validRegions]
        assert (regionIndex in validRegionNumbers), "Invalid Region requested: valid regions are {}".format(
            validRegionNumbers)
        if beQuiet == False: print("Loading region #{} of {}".format(regionIndex, len(validRegions)))

        # Check for the presence of a third data dimension in the selected region
        # Unfortunately this is necessary because SES doesn't write a 'run-mode' entry for manipulator scans.
        # We do this by assuming that the Dimension 3 scale information always comes at a fixed offset from the Region start line.
        # If you go to that line and find it blank, it's because SES didn't write anything about a third dimension.

        testLine = linecache.getline(fileName, validRegions[regionIndex - 1][1] + 9)
        if testLine.startswith("Dimension 3"):
            loadedSpectrum['Axis'].append([])
            loadedSpectrum['AxisLabel'].append("")
            loadedSpectrum['AxisUnits'].append("")

            numImages = int(linecache.getline(fileName, validRegions[regionIndex - 1][1] + 10).split("=")[1])
            numAngleSteps = int(linecache.getline(fileName, validRegions[regionIndex - 1][1] + 7).split("=")[1])
            numEnergySteps = int(linecache.getline(fileName, validRegions[regionIndex - 1][1] + 4).split("=")[1])
            loadedSpectrum['data'] = np.zeros((numEnergySteps, numAngleSteps, numImages), np.float32)

            if testLine.split("=")[1].startswith("Photon"):
                fileType = 'ARPES photon energy scan'
                if beQuiet == False:
                    print("Inferring from the presence and name of dimension 3 that this is a photon energy scan")

            else:
                fileType = '1D Manipulator Scan'
                if beQuiet == False:
                    print("Inferring from the presence of an unnamed dimension 3 that this is a 1D manipulator scan")

                e, a, m, m_name, m_units = get_txt_manipulatorscan_axis(fileName=fileName,
                                                                        whichManipulatorAxis='',
                                                                        fixRounding=True,
                                                                        regionIndex=validRegions[regionIndex - 1][0],
                                                                        beQuiet=False)
                loadedSpectrum['Axis'][2] = m
                loadedSpectrum['AxisLabel'][2] = m_name
                loadedSpectrum['AxisUnits'][2] = m_units
                loadedSpectrum['data'] = np.zeros((len(e), len(a), len(m)))





        else:
            if beQuiet == False: print("Inferring from data dimension that this is single 2D image")
            fileType = '2D image'
            numAngleSteps = int(linecache.getline(fileName, validRegions[regionIndex - 1][1] + 7).split("=")[1])
            numEnergySteps = int(linecache.getline(fileName, validRegions[regionIndex - 1][1] + 4).split("=")[1])
            loadedSpectrum['data'] = np.zeros((numEnergySteps, numAngleSteps), np.float32)

        regionStartLine = validRegions[regionIndex - 1][1]
        dataStartLine = 9999999
        loadedSpectrum['Axis'][0] = []
        frameNumber = 0
        with open(fileName) as fp:
            for linenumber, line in enumerate(fp):
                if linenumber < regionStartLine:
                    pass
                elif fileType == '2D image' and linenumber > (
                        dataStartLine + len(loadedSpectrum['Axis'][0])):  # data load finished
                    break
                elif (
                        fileType == '1D Manipulator Scan' or fileType == 'ARPES photon energy scan') and frameNumber == numImages:  # data load (Photon energy scan or 1D manipulator scan) finished
                    break
                else:
                    if line.startswith("Dimension 1 scale="):  # Energy axis scale
                        Axis = line.rstrip('\n').split("=")[1].split(" ")
                        loadedSpectrum['Axis'][0] = [float(ii) for ii in Axis]

                    if line.startswith("Dimension 1 name="):
                        AxisLabel = line.rstrip('\n').split("=")[1]
                        if AxisLabel.startswith("Binding"):
                            loadedSpectrum['AxisLabel'][0] = "Binding energy"
                            loadedSpectrum['AxisUnits'][0] = "eV"
                        else:
                            loadedSpectrum['AxisLabel'][0] = "Kinetic energy"
                            loadedSpectrum['AxisUnits'][0] = "eV"

                    if line.startswith("Dimension 2 scale="):  # Angle axis scale
                        Axis = line.rstrip('\n').split("=")[1].split(" ")
                        loadedSpectrum['Axis'][1] = [float(ii) for ii in Axis]
                        loadedSpectrum['AxisLabel'][1] = "Angle"
                        loadedSpectrum['AxisUnits'][1] = "$\degree$"
                        if fileType == '2D image':
                            loadedSpectrum['data'] = np.zeros(
                                (len(loadedSpectrum['Axis'][0]), len(loadedSpectrum['Axis'][1])))

                    if line.startswith("Dimension 3 scale=") and fileType == 'ARPES photon energy scan':
                        zAxis = line.rstrip('\n').split("=")[1].split(" ")
                        loadedSpectrum['Axis'][2] = [float(ii) for ii in zAxis]
                        loadedSpectrum['AxisLabel'][2] = "Photon energy"
                        loadedSpectrum['AxisUnits'][2] = "eV"
                        loadedSpectrum['data'] = np.zeros((len(loadedSpectrum['Axis'][0]),
                                                           len(loadedSpectrum['Axis'][1]),
                                                           len(loadedSpectrum['zAxis'][2])))

                    if line.startswith("Region Name="): loadedSpectrum['Metadata']['Region Name'] = line.replace(
                        "Region Name=", "").rstrip('\n')
                    if line.startswith("Lens Mode="): loadedSpectrum['Metadata']['Lens Mode'] = line.replace(
                        "Lens Mode=", "").rstrip('\n')
                    if line.startswith("Pass Energy="): loadedSpectrum['Metadata']['Pass Energy'] = int(
                        line.replace("Pass Energy=", "").rstrip('\n'))
                    if line.startswith("Number of Sweeps="): loadedSpectrum['Metadata']['Number of Sweeps'] = int(
                        line.replace("Number of Sweeps=", "").rstrip('\n'))
                    if line.startswith("Excitation Energy="): loadedSpectrum['Metadata']['Excitation Energy'] = float(
                        line.replace("Excitation Energy=", "").rstrip('\n').replace(",", "."))
                    if line.startswith("Energy Scale="): loadedSpectrum['Metadata']['Energy Scale'] = line.replace(
                        "Energy Scale=", "").rstrip('\n')
                    if line.startswith("Acquisition Mode="): loadedSpectrum['Metadata'][
                        'Acquisition Mode'] = line.replace("Acquisition Mode=", "").rstrip('\n')
                    if line.startswith("Energy Unit="): loadedSpectrum['Metadata']['Energy Unit'] = line.replace(
                        "Energy Unit=", "").rstrip('\n')
                    if line.startswith("Center Energy="): loadedSpectrum['Metadata']['Center Energy'] = float(
                        line.replace("Center Energy=", "").rstrip('\n').replace(",", "."))
                    if line.startswith("Low Energy="): loadedSpectrum['Metadata']['Low Energy'] = float(
                        line.replace("Low Energy=", "").rstrip('\n').replace(",", "."))
                    if line.startswith("High Energy="): loadedSpectrum['Metadata']['High Energy'] = float(
                        line.replace("High Energy=", "").rstrip('\n').replace(",", "."))
                    if line.startswith("Energy Step="): loadedSpectrum['Metadata']['Energy Step'] = float(
                        line.replace("Energy Step=", "").rstrip('\n').replace(",", "."))
                    if line.startswith("Step Time="): loadedSpectrum['Metadata']['Step Time'] = float(
                        line.replace("Step Time=", "").rstrip('\n').replace(",", "."))
                    if line.startswith("Detector First X-Channel="): loadedSpectrum['Metadata'][
                        'Detector First X-Channel'] = int(line.replace("Detector First X-Channel=", "").rstrip('\n'))
                    if line.startswith("Detector Last X-Channel="): loadedSpectrum['Metadata'][
                        'Detector Last X-Channel'] = int(line.replace("Detector Last X-Channel=", "").rstrip('\n'))
                    if line.startswith("Detector First Y-Channel="): loadedSpectrum['Metadata'][
                        'Detector First Y-Channel'] = int(line.replace("Detector First Y-Channel=", "").rstrip('\n'))
                    if line.startswith("Detector Last Y-Channel="): loadedSpectrum['Metadata'][
                        'Detector Last Y-Channel'] = int(line.replace("Detector Last Y-Channel=", "").rstrip('\n'))
                    if line.startswith("Number of Slices="): loadedSpectrum['Metadata']['Number of Slices'] = int(
                        line.replace("Number of Slices=", "").rstrip('\n'))
                    if line.startswith("File="):
                        path = line.replace("File=", "").rstrip('\n').replace("\\\\", "\\")
                        loadedSpectrum['Metadata']['File Path'] = path[:path.rindex('\\') + 1] + fileName
                    if line.startswith("Sequence="): loadedSpectrum['Metadata']['Sequence'] = line.replace("Sequence=",
                                                                                                           "").rstrip(
                        '\n').replace("\\\\", "\\")
                    if line.startswith("Spectrum Name="): loadedSpectrum['Metadata']['Spectrum Name'] = line.replace(
                        "Spectrum Name=", "").rstrip('\n')
                    if line.startswith("Instrument="): loadedSpectrum['Metadata']['Instrument'] = line.replace(
                        "Instrument=", "").rstrip('\n')
                    if line.startswith("Location="): loadedSpectrum['Metadata']['Location'] = line.replace("Location=",
                                                                                                           "").rstrip(
                        '\n')
                    if line.startswith("User="): loadedSpectrum['Metadata']['User'] = line.replace("User=", "").rstrip(
                        '\n')
                    if line.startswith("Sample="): loadedSpectrum['Metadata']['Sample'] = line.replace("Sample=",
                                                                                                       "").rstrip('\n')
                    if line.startswith("Comments="): loadedSpectrum['Metadata']['Comments'] = line.replace("Comments=",
                                                                                                           "").rstrip(
                        '\n')
                    if line.startswith("Date="): loadedSpectrum['Metadata']['Date'] = line.replace("Date=", "").rstrip(
                        '\n')
                    if line.startswith("Time="): loadedSpectrum['Metadata']['Time'] = line.replace("Time=", "").rstrip(
                        '\n')
                    if line.startswith("Time per Spectrum Channel="): loadedSpectrum['Metadata'][
                        'Time per Spectrum Channel'] = float(
                        line.replace("Time per Spectrum Channel=", "").rstrip('\n').replace(",", "."))
                    if line.startswith("DetectorMode="): loadedSpectrum['Metadata']['DetectorMode'] = line.replace(
                        "DetectorMode=", "").rstrip('\n')

                    if fileType != '1D Manipulator Scan':
                        if line.startswith("A="): loadedSpectrum['Metadata']['Manipulator Azimuth'] = float(
                            line.replace("A=", "").rstrip('\n').replace(",", "."))
                        if line.startswith("P="): loadedSpectrum['Metadata']['Manipulator Polar'] = float(
                            line.replace("P=", "").rstrip('\n').replace(",", "."))
                        if line.startswith("T="): loadedSpectrum['Metadata']['Manipulator Tilt'] = float(
                            line.replace("T=", "").rstrip('\n').replace(",", "."))
                        if line.startswith("X="): loadedSpectrum['Metadata']['Manipulator X'] = float(
                            line.replace("X=", "").rstrip('\n').replace(",", "."))
                        if line.startswith("Y="): loadedSpectrum['Metadata']['Manipulator Y'] = float(
                            line.replace("Y=", "").rstrip('\n').replace(",", "."))
                        if line.startswith("Z="): loadedSpectrum['Metadata']['Manipulator Z'] = float(
                            line.replace("Z=", "").rstrip('\n').replace(",", "."))

                    if (
                            fileType == 'ARPES photon energy scan' or fileType == '1D Manipulator Scan') and line.startswith(
                            "[Data {}:".format(regionIndex)):
                        loadedImage = np.genfromtxt(fp, delimiter='  ', skip_header=0,
                                                    max_rows=len(loadedSpectrum['Axis'][0]))
                        loadedSpectrum['data'][:, :, frameNumber] = loadedImage[:, 1:]
                        frameNumber += 1
                        if beQuiet == False: printProgressBar(frameNumber, len(loadedSpectrum['Axis'][2]),
                                                              prefix='Progress:', suffix='Complete', length=50)

                    if fileType == '2D image' and line.startswith("[Data {}]".format(regionIndex)):
                        dataStartLine = linenumber + 1

                    if fileType == '2D image':
                        if linenumber >= dataStartLine and linenumber < (
                                dataStartLine + len(loadedSpectrum['Axis'][0])):

                            cps = line.rstrip('\n').split("  ")
                            for index, element in enumerate(cps):
                                if index == 0:
                                    pass
                                else:
                                    loadedSpectrum['data'][linenumber - dataStartLine][index - 1] = float(element)

        return loadedSpectrum

    def load_ibw(fileName, beQuiet=False, regionIndex=1, whichManipulatorAxis=''):
        loadedSpectrum = {}
        loadedSpectrum['Metadata'] = {}
        loadedSpectrum['Axis'] = [[], []]
        loadedSpectrum['AxisLabel'] = ["", ""]
        loadedSpectrum['AxisUnits'] = ["", ""]

        t = binarywave.load(fileName)

        """ Figure out what kind of measurement file this is. Unfortunately SES is not consistent 
        about using the 'RunMode' metadata entry, so we have to make some inferences based on data dimensions.

        t['wave']['wave_header']['nDim'] is always 4 elements long, but unused dimensions will be set to zero.
        So the dimensionality of the dataset is the number of non-zero elements in ['nDim']
        """

        if t['wave']['wave_header']['nDim'][2] == 0 and t['wave']['wave_header']['nDim'][
            3] == 0:  # Only 2 of 4 dimensions used
            if beQuiet == False: print("2D dataset, must be an image")
            numDataDimensions = 2
            fileType = '2D image'

        elif t['wave']['wave_header']['nDim'][3] == 0:  # Only 3 of 4 dimensions unused
            if beQuiet == False: print("3D dataset, I think it's...")
            numDataDimensions = 3

            loadedSpectrum['Axis'] = [[], [], []]
            loadedSpectrum['AxisLabel'] = ["", "", ""]
            loadedSpectrum['AxisUnits'] = ["", "", ""]

            RunMode = str(t['wave']['note']).replace("\\r", "\n").split('[Run Mode Information]')[1].splitlines()[
                1].replace("Name=", "")
            LensMode = str(t['wave']['note']).replace("\\r", "\n").split('Lens Mode=')[1].splitlines()[0]
            if RunMode == 'CIS':
                if LensMode == 'Transmission':
                    fileType = 'XPS photon energy scan'
                else:
                    fileType = 'ARPES photon energy scan'

            else:
                fileType = '1D Manipulator Scan'

                numImages = t['wave']['wave_header']['nDim'][2]
                manipulatorInfoStartLine = 99999
                loadedSpectrum['Metadata']['Manipulator Axis Names'] = []
                loadedSpectrum['Metadata']['Manipulator Axis Units'] = []
                loadedSpectrum['Metadata']['Manipulator Axes'] = []

            if beQuiet == False: print(fileType)

        else:  # All four dimensions used
            if beQuiet == False: print("4D dataset, must be a 2D manipulator scan")
            if beQuiet == False: print("Detector image size is {} energy pixels and {} angle pixels".format(
                t['wave']['wave_header']['nDim'][0], t['wave']['wave_header']['nDim'][1]))
            numDataDimensions = 4
            loadedSpectrum['Axis'] = [[], [], [], []]
            loadedSpectrum['AxisLabel'] = ["", "", "", ""]
            loadedSpectrum['AxisUnits'] = ["", "", "", ""]

            fileType = '2D Manipulator Scan'
            numImages = t['wave']['wave_header']['nDim'][2] * t['wave']['wave_header']['nDim'][3]

            manipulatorInfoStartLine = 99999
            loadedSpectrum['Metadata']['Manipulator Axis Names'] = []
            loadedSpectrum['Metadata']['Manipulator Axis Units'] = []
            loadedSpectrum['Metadata']['Manipulator Axes'] = []

        """ Every scan type has the two dimensions corresponding to the detector image.
        Retrieve this information now"""

        energyStart = t['wave']['wave_header']['sfB'][0]
        energyStart = float(np.format_float_positional(energyStart, precision=5))
        angleStart = t['wave']['wave_header']['sfB'][1]
        energyStep = t['wave']['wave_header']['sfA'][0]
        energyStep = float(np.format_float_positional(energyStep, precision=5))
        angleStep = t['wave']['wave_header']['sfA'][1]

        numEnergySteps = t['wave']['wave_header']['nDim'][0]
        numAngleSteps = t['wave']['wave_header']['nDim'][1]
        energyEnd = energyStart + (energyStep * (numEnergySteps - 1))
        angleEnd = angleStart + (angleStep * (numAngleSteps - 1))

        loadedSpectrum['Axis'][1] = np.linspace(angleStart, angleEnd, numAngleSteps)
        loadedSpectrum['AxisLabel'][1] = "Angle"
        loadedSpectrum['AxisUnits'][1] = "$\degree$"

        loadedSpectrum['Axis'][0] = np.linspace(energyStart, energyEnd, numEnergySteps)

        if str(t['wave']['dimension_units']).startswith("b'Binding"):
            loadedSpectrum['AxisLabel'][0] = "Binding energy"
            loadedSpectrum['AxisUnits'][0] = "eV"
        else:
            loadedSpectrum['AxisLabel'][0] = "Kinetic energy"
            loadedSpectrum['AxisUnits'][0] = "eV"

        """ Get the n-dimensional dataset:"""
        # Note: Our convention here is:
        # Dimension 0: energy
        # Dimension 1: analyzer angle
        # Dimension 2: polar angle / hv / deflector angle
        # Getting it like this requires(?) some transposition:

        if len(np.shape(t['wave']['wData'])) == 2:
            loadedSpectrum['data'] = t['wave']['wData']  # .transpose(1,0)
        elif len(np.shape(t['wave']['wData'])) == 3:
            loadedSpectrum['data'] = t['wave']['wData'].transpose(0, 1, 2)
        elif len(np.shape(t['wave']['wData'])) == 4:
            loadedSpectrum['data'] = t['wave']['wData'].transpose(0, 1, 2, 3)

        """ Parse and load the metadata:"""
        for linenumber, line in enumerate((str(t['wave']['note']).replace("\\r", "\n")).splitlines()):
            # print(line)
            if line.startswith("Region Name="): loadedSpectrum['Metadata']['Region Name'] = line.replace("Region Name=",
                                                                                                         "").rstrip(
                '\n')
            if line.startswith("Lens Mode="): loadedSpectrum['Metadata']['Lens Mode'] = line.replace("Lens Mode=",
                                                                                                     "").rstrip('\n')
            if line.startswith("Pass Energy="): loadedSpectrum['Metadata']['Pass Energy'] = int(
                line.replace("Pass Energy=", "").rstrip('\n'))
            if line.startswith("Number of Sweeps="): loadedSpectrum['Metadata']['Number of Sweeps'] = int(
                line.replace("Number of Sweeps=", "").rstrip('\n'))
            if line.startswith("Excitation Energy="): loadedSpectrum['Metadata']['Excitation Energy'] = float(
                line.replace("Excitation Energy=", "").rstrip('\n').replace(",", "."))
            if line.startswith("Energy Scale="): loadedSpectrum['Metadata']['Energy Scale'] = line.replace(
                "Energy Scale=", "").rstrip('\n')
            if line.startswith("Acquisition Mode="): loadedSpectrum['Metadata']['Acquisition Mode'] = line.replace(
                "Acquisition Mode=", "").rstrip('\n')
            if line.startswith("Energy Unit="): loadedSpectrum['Metadata']['Energy Unit'] = line.replace("Energy Unit=",
                                                                                                         "").rstrip(
                '\n')
            if line.startswith("Center Energy="): loadedSpectrum['Metadata']['Center Energy'] = float(
                line.replace("Center Energy=", "").rstrip('\n').replace(",", "."))
            if line.startswith("Low Energy="): loadedSpectrum['Metadata']['Low Energy'] = float(
                line.replace("Low Energy=", "").rstrip('\n').replace(",", "."))
            if line.startswith("High Energy="): loadedSpectrum['Metadata']['High Energy'] = float(
                line.replace("High Energy=", "").rstrip('\n').replace(",", "."))
            if line.startswith("Energy Step="): loadedSpectrum['Metadata']['Energy Step'] = float(
                line.replace("Energy Step=", "").rstrip('\n').replace(",", "."))
            if line.startswith("Step Time="): loadedSpectrum['Metadata']['Step Time'] = float(
                line.replace("Step Time=", "").rstrip('\n').replace(",", "."))
            if line.startswith("Detector First X-Channel="): loadedSpectrum['Metadata'][
                'Detector First X-Channel'] = int(line.replace("Detector First X-Channel=", "").rstrip('\n'))
            if line.startswith("Detector Last X-Channel="): loadedSpectrum['Metadata']['Detector Last X-Channel'] = int(
                line.replace("Detector Last X-Channel=", "").rstrip('\n'))
            if line.startswith("Detector First Y-Channel="): loadedSpectrum['Metadata'][
                'Detector First Y-Channel'] = int(line.replace("Detector First Y-Channel=", "").rstrip('\n'))
            if line.startswith("Detector Last Y-Channel="): loadedSpectrum['Metadata']['Detector Last Y-Channel'] = int(
                line.replace("Detector Last Y-Channel=", "").rstrip('\n'))
            if line.startswith("Number of Slices="): loadedSpectrum['Metadata']['Number of Slices'] = int(
                line.replace("Number of Slices=", "").rstrip('\n'))
            if line.startswith("File="):
                path = line.replace("File=", "").rstrip('\n').replace("\\\\", "\\")
                loadedSpectrum['Metadata']['File Path'] = path[:path.rindex('\\') + 1] + fileName
            if line.startswith("Sequence="): loadedSpectrum['Metadata']['Sequence'] = line.replace("Sequence=",
                                                                                                   "").rstrip(
                '\n').replace("\\\\", "\\")
            if line.startswith("Spectrum Name="): loadedSpectrum['Metadata']['Spectrum Name'] = line.replace(
                "Spectrum Name=", "").rstrip('\n')
            if line.startswith("Instrument="): loadedSpectrum['Metadata']['Instrument'] = line.replace("Instrument=",
                                                                                                       "").rstrip('\n')
            if line.startswith("Location="): loadedSpectrum['Metadata']['Location'] = line.replace("Location=",
                                                                                                   "").rstrip('\n')
            if line.startswith("User="): loadedSpectrum['Metadata']['User'] = line.replace("User=", "").rstrip('\n')
            if line.startswith("Sample="): loadedSpectrum['Metadata']['Sample'] = line.replace("Sample=", "").rstrip(
                '\n')
            if line.startswith("Comments="): loadedSpectrum['Metadata']['Comments'] = line.replace("Comments=",
                                                                                                   "").rstrip('\n')
            if line.startswith("Date="): loadedSpectrum['Metadata']['Date'] = line.replace("Date=", "").rstrip('\n')
            if line.startswith("Time="): loadedSpectrum['Metadata']['Time'] = line.replace("Time=", "").rstrip('\n')
            if line.startswith("Time per Spectrum Channel="): loadedSpectrum['Metadata'][
                'Time per Spectrum Channel'] = float(
                line.replace("Time per Spectrum Channel=", "").rstrip('\n').replace(",", "."))
            if line.startswith("DetectorMode="): loadedSpectrum['Metadata']['DetectorMode'] = line.replace(
                "DetectorMode=", "").rstrip('\n')

            if fileType != '1D Manipulator Scan' and fileType != '2D Manipulator Scan':  # Unless it's a manipulator scan, there is one constant set of manipulator values
                if line.startswith("A="): loadedSpectrum['Metadata']['Manipulator Azimuth'] = float(
                    line.replace("A=", "").rstrip('\n').replace(",", "."))
                if line.startswith("P="): loadedSpectrum['Metadata']['Manipulator Polar'] = float(
                    line.replace("P=", "").rstrip('\n').replace(",", "."))
                if line.startswith("T="): loadedSpectrum['Metadata']['Manipulator Tilt'] = float(
                    line.replace("T=", "").rstrip('\n').replace(",", "."))
                if line.startswith("X="): loadedSpectrum['Metadata']['Manipulator X'] = float(
                    line.replace("X=", "").rstrip('\n').replace(",", "."))
                if line.startswith("Y="): loadedSpectrum['Metadata']['Manipulator Y'] = float(
                    line.replace("Y=", "").rstrip('\n').replace(",", "."))
                if line.startswith("Z="): loadedSpectrum['Metadata']['Manipulator Z'] = float(
                    line.replace("Z=", "").rstrip('\n').replace(",", "."))

            if fileType == 'XPS photon energy scan' or fileType == 'ARPES photon energy scan':
                hvStart = t['wave']['wave_header']['sfB'][2]
                hvStep = t['wave']['wave_header']['sfA'][2]
                numhvSteps = t['wave']['wave_header']['nDim'][2]
                hvEnd = hvStart + (hvStep * (numhvSteps - 1))
                loadedSpectrum['Axis'][2] = np.linspace(hvStart, hvEnd, numhvSteps)
                loadedSpectrum['AxisLabel'][2] = "Photon energy"
                loadedSpectrum['AxisUnits'][2] = "eV"

            if fileType == '1D Manipulator Scan' or fileType == '2D Manipulator Scan':

                if line.startswith("[Run Mode Information]"):
                    manipulatorInfoStartLine = linenumber + 1
                    if fileType == '2D Manipulator Scan': manipulatorInfoStartLine += 1  # For some reason 2D maps include an extra 'Name=Manipulator Scan' element that 1D maps don't have. Offset by one to skip past it to the axis names.

                if linenumber == manipulatorInfoStartLine:
                    for axisName in line.split('\\x0b'):
                        axisNames = {}
                        axisNames['A'] = 'Azimuth'
                        axisNames['P'] = 'Polar'
                        axisNames['T'] = 'Tilt'
                        axisUnits = {}
                        axisUnits['A'] = '$\degree$'
                        axisUnits['P'] = '$\degree$'
                        axisUnits['T'] = '$\degree$'
                        axisUnits['X'] = 'mm'
                        axisUnits['Y'] = 'mm'
                        axisUnits['Z'] = 'mm'
                        try:
                            loadedSpectrum['Metadata']['Manipulator Axis Names'].append(
                                axisNames[axisName.rstrip('\n')])
                        except KeyError:
                            loadedSpectrum['Metadata']['Manipulator Axis Names'].append(axisName.rstrip('\n'))

                        try:
                            loadedSpectrum['Metadata']['Manipulator Axis Units'].append(
                                axisUnits[axisName.rstrip('\n')])
                        except KeyError:
                            loadedSpectrum['Metadata']['Manipulator Axis Units'].append("")

                        loadedSpectrum['Metadata']['Manipulator Axes'].append([])

                    if beQuiet == False: print("I found the following axis names in the metadata:",
                                               [ii for ii in loadedSpectrum['Metadata']['Manipulator Axis Names']])

                if linenumber > manipulatorInfoStartLine and linenumber <= manipulatorInfoStartLine + numImages:
                    data = line.split('\\x0b')
                    # print(data)
                    for index, axisName in enumerate(loadedSpectrum['Metadata']['Manipulator Axis Names']):
                        loadedSpectrum['Metadata']['Manipulator Axes'][index].append(float(data[index]))

        if fileType == '2D Manipulator Scan':
            """ 
            The 'position' axis holds the value of the outer loop axis variable (major)
            The 'point' axis holds the value of the inner loop variable (minor)

            This lets us deduce the names of the scanned axes
            """

            numAxes = len(loadedSpectrum['Metadata']['Manipulator Axis Names'])
            majorAxis = loadedSpectrum['Metadata']['Manipulator Axes'][0]
            minorAxis = loadedSpectrum['Metadata']['Manipulator Axes'][1]
            lenMajorAxis = t['wave']['wave_header']['nDim'][3]
            lenMinorAxis = t['wave']['wave_header']['nDim'][2]

            if loadedSpectrum['Metadata']['Manipulator Axis Names'][0] == 'Position [au]':
                print(
                    "(!!) Something was not right with your scan settings. Check that you assigned unique inner and outer loop variables.")
            if loadedSpectrum['Metadata']['Manipulator Axis Names'][1] == 'Point [au]':
                print(
                    "(!!) Something was not right with your scan settings. Check that you assigned unique inner and outer loop variables.")

            minorAxis = loadedSpectrum['Metadata']['Manipulator Axes'][1]
            lenMajorAxis = t['wave']['wave_header']['nDim'][3]
            lenMinorAxis = t['wave']['wave_header']['nDim'][2]

            foundMajorAxis = False
            foundMinorAxis = False
            for index, name in enumerate(loadedSpectrum['Metadata']['Manipulator Axis Names'][2:]):
                if loadedSpectrum['Metadata']['Manipulator Axes'][index + 2] == majorAxis:
                    loadedSpectrum['majorAxisLabel'] = name
                    loadedSpectrum['majorAxis'] = majorAxis[0::lenMinorAxis]
                    foundMajorAxis = True
                if loadedSpectrum['Metadata']['Manipulator Axes'][index + 2] == minorAxis:
                    loadedSpectrum['minorAxisLabel'] = name

                    loadedSpectrum['minorAxis'] = minorAxis[0:lenMinorAxis]
                    foundMinorAxis = True
            if foundMajorAxis == False:
                print("Can't identify a major scan axis, so won't be able to do anything with this. Sorry :(")
            if foundMinorAxis == False:
                print("Can't identify a minor scan axis, so won't be able to do anything with this. Sorry :(")

            foundMajorAxis = False
            foundMinorAxis = False
            for index, name in enumerate(loadedSpectrum['Metadata']['Manipulator Axis Names'][2:]):
                if loadedSpectrum['Metadata']['Manipulator Axes'][index + 2] == majorAxis:
                    loadedSpectrum['majorAxisLabel'] = name
                    loadedSpectrum['majorAxis'] = majorAxis[0::lenMinorAxis]
                    foundMajorAxis = True
                if loadedSpectrum['Metadata']['Manipulator Axes'][index + 2] == minorAxis:
                    loadedSpectrum['minorAxisLabel'] = name

                    loadedSpectrum['minorAxis'] = minorAxis[0:lenMinorAxis]
                    foundMinorAxis = True
            if foundMajorAxis == False or foundMinorAxis == False:
                print("File is corrupted, sorry :(")
            else:
                majorLength = len(loadedSpectrum['majorAxis'])
                majorStep = (loadedSpectrum['majorAxis'][-1] - loadedSpectrum['majorAxis'][0]) / (
                            len(loadedSpectrum['majorAxis']) - 1)
                majorStart = loadedSpectrum['majorAxis'][0]
                loadedSpectrum['majorAxis'] = [majorStart + ii * majorStep for ii in range(majorLength)]
                loadedSpectrum['Axis'][2] = loadedSpectrum['majorAxis']
                loadedSpectrum['AxisLabel'][2] = loadedSpectrum['majorAxisLabel']
                if loadedSpectrum['AxisLabel'][2] == "X" or loadedSpectrum['AxisLabel'][2] == "Y":
                    loadedSpectrum['AxisUnits'][2] = "mm"
                else:
                    loadedSpectrum['AxisUnits'][2] = "$\degree$"
                minorLength = len(loadedSpectrum['minorAxis'])
                minorStep = (loadedSpectrum['minorAxis'][-1] - loadedSpectrum['minorAxis'][0]) / (
                            len(loadedSpectrum['minorAxis']) - 1)
                minorStart = loadedSpectrum['minorAxis'][0]
                loadedSpectrum['minorAxis'] = [minorStart + ii * minorStep for ii in range(minorLength)]

                loadedSpectrum['Axis'][3] = loadedSpectrum['minorAxis']
                loadedSpectrum['AxisLabel'][3] = loadedSpectrum['minorAxisLabel']
                if loadedSpectrum['AxisLabel'][3] == "X" or loadedSpectrum['AxisLabel'][3] == "Y":
                    loadedSpectrum['AxisUnits'][3] = "mm"
                else:
                    loadedSpectrum['AxisUnits'][3] = "$\degree$"

                if beQuiet == False: print(
                    "Major scan axis was {}, from {:.3f} to {:.3f} step {:.4f} ({} points)".format(
                        loadedSpectrum['majorAxisLabel'], loadedSpectrum['majorAxis'][0],
                        loadedSpectrum['majorAxis'][-1],
                        loadedSpectrum['majorAxis'][1] - loadedSpectrum['majorAxis'][0],
                        len(loadedSpectrum['majorAxis'])))
                if beQuiet == False: print(
                    "Minor scan axis was {}, from {:.3f} to {:.3f} step {:.4f} ({} points)".format(
                        loadedSpectrum['minorAxisLabel'], loadedSpectrum['minorAxis'][0],
                        loadedSpectrum['minorAxis'][-1],
                        loadedSpectrum['minorAxis'][1] - loadedSpectrum['minorAxis'][0],
                        len(loadedSpectrum['minorAxis'])))
                if beQuiet == False: print("Recalculated axis values, since SES header rounds to 3 decimal places")

        if fileType == '1D Manipulator Scan':

            manipulatorScannedAxisNames = []
            numberOfScannedAxes = 0
            for linenumber, axisName in enumerate(loadedSpectrum['Metadata']['Manipulator Axis Names']):
                if loadedSpectrum['Metadata']['Manipulator Axes'][linenumber][-1] != \
                        loadedSpectrum['Metadata']['Manipulator Axes'][linenumber][0]:
                    numberOfScannedAxes += 1
                    manipulatorScannedAxisNames.append(axisName)
                    # if beQuiet==False: print(loadedSpectrum['Metadata']['Manipulator Axes'][linenumber])
                    stepSize = abs(loadedSpectrum['Metadata']['Manipulator Axes'][linenumber][-1] -
                                   loadedSpectrum['Metadata']['Manipulator Axes'][linenumber][0]) / (numImages - 1)
                    if beQuiet == False: print(
                        "Axis \'{}\' was scanned from {} to {} in steps of {:.5f}".format(axisName,
                                                                                          loadedSpectrum['Metadata'][
                                                                                              'Manipulator Axes'][
                                                                                              linenumber][0],
                                                                                          loadedSpectrum['Metadata'][
                                                                                              'Manipulator Axes'][
                                                                                              linenumber][-1],
                                                                                          stepSize))
                elif beQuiet == False:
                    print("Axis \'{}\' was {}".format(axisName,
                                                      loadedSpectrum['Metadata']['Manipulator Axes'][linenumber][0]))

            if numberOfScannedAxes == 1:
                # print("\nInferring that the primary manipulator scan axis must be:",[ii for ii in manipulatorScannedAxisNames])
                primaryAxislinenumber = loadedSpectrum['Metadata']['Manipulator Axis Names'].index(
                    manipulatorScannedAxisNames[0])
                primaryManipulatorAxis = loadedSpectrum['Metadata']['Manipulator Axes'][primaryAxislinenumber]


            elif whichManipulatorAxis in manipulatorScannedAxisNames:
                if beQuiet == False: print("Treating {} as the primary scan axis".format(whichManipulatorAxis))
                primaryAxislinenumber = loadedSpectrum['Metadata']['Manipulator Axis Names'].index(whichManipulatorAxis)
                primaryManipulatorAxis = loadedSpectrum['Metadata']['Manipulator Axes'][primaryAxislinenumber]

            elif numberOfScannedAxes == 2:
                if beQuiet == False: print(
                    "There are two scanned axes, but one of them is just the index. Treating {} as the primary scan axis".format(
                        manipulatorScannedAxisNames[1]))
                primaryAxislinenumber = loadedSpectrum['Metadata']['Manipulator Axis Names'].index(
                    manipulatorScannedAxisNames[1])
                primaryManipulatorAxis = loadedSpectrum['Metadata']['Manipulator Axes'][primaryAxislinenumber]

            else:
                if (whichManipulatorAxis == ''):
                    if beQuiet == False: print("\n-------- !! WARNING !! ---------")
                    if beQuiet == False: print(
                        "I don't know which of these is the primary manipulator scan axis, and you didn't specify.")
                    if beQuiet == False: print("I will therefore assume that the primary axis is ",
                                               manipulatorScannedAxisNames[0])
                    if beQuiet == False: print(
                        "To choose a different primary axis, please call this function again, passing the additional argument whichManipulatorAxis=n, where n is one of:",
                        [ii for ii in manipulatorScannedAxisNames])
                    if beQuiet == False: print("---------------------------------")
                    primaryAxislinenumber = loadedSpectrum['Metadata']['Manipulator Axis Names'].index(
                        manipulatorScannedAxisNames[0])
                    primaryManipulatorAxis = loadedSpectrum['Metadata']['Manipulator Axes'][primaryAxislinenumber]

                else:
                    if beQuiet == False: print("\n-------- !! WARNING !! ---------")
                    if beQuiet == False: print(
                        "The name of the primary manipulator scan axis that you provided ({}) is not the name of a scanned axis ({}).".format(
                            whichManipulatorAxis, manipulatorScannedAxisNames))
                    if beQuiet == False: print("I will therefore assume that the primary axis is ",
                                               manipulatorScannedAxisNames[0])
                    if beQuiet == False: print(
                        "To choose a different primary axis, please call this function again, passing the additional argument whichManipulatorAxis=n, where n is one of:",
                        [ii for ii in manipulatorScannedAxisNames])
                    if beQuiet == False: print("---------------------------------")
                    primaryAxislinenumber = loadedSpectrum['Metadata']['Manipulator Axis Names'].index(
                        manipulatorScannedAxisNames[0])
                    primaryManipulatorAxis = loadedSpectrum['Metadata']['Manipulator Axes'][primaryAxislinenumber]

            if beQuiet == False: print("Recalculated axis values, since SES header rounds to 3 decimal places")
            axisStepSize = (loadedSpectrum['Metadata']['Manipulator Axes'][primaryAxislinenumber][-1] -
                            loadedSpectrum['Metadata']['Manipulator Axes'][primaryAxislinenumber][0]) / (numImages - 1)
            axisStart = loadedSpectrum['Metadata']['Manipulator Axes'][primaryAxislinenumber][0]
            loadedSpectrum['Axis'][2] = [axisStart + ii * axisStepSize for ii in range(numImages)]
            loadedSpectrum['AxisLabel'][2] = loadedSpectrum['Metadata']['Manipulator Axis Names'][primaryAxislinenumber]
            loadedSpectrum['AxisUnits'][2] = loadedSpectrum['Metadata']['Manipulator Axis Units'][primaryAxislinenumber]

        return loadedSpectrum

    def load_zip(fileName, beQuiet=False, regionIndex=1):

        regionNames = []

        with zipfile.ZipFile(fileName) as z:
            for filename in z.namelist():
                if filename.startswith('Spectrum_') and filename.endswith('.bin'):
                    regionNames.append(filename.replace("Spectrum_", "").replace(".bin", ""))

            if regionIndex > len(regionNames):
                if len(regionNames) == 1:
                    assert False, "This zip file contains only a single region, with index 1. You asked for region #{}, which doesn't exist.".format(
                        regionIndex)
                else:
                    assert False, "This zip file contains regions with indices spanning (1..{}). You asked for region #{}, which doesn't exist.".format(
                        len(regionNames) - 1, regionIndex)
                return

            loadedSpectrum = {}
            loadedSpectrum['Metadata'] = {}
            loadedSpectrum['Axis'] = [[], [], []]
            loadedSpectrum['AxisLabel'] = ["", "", ""]
            loadedSpectrum['AxisUnits'] = ["", "", ""]

            filename = 'Spectrum_' + regionNames[regionIndex - 1] + '.ini'

            with z.open(filename) as f:

                for l in f:
                    line = l.decode()

                    # ---- Energy axis ----
                    if line.startswith("widthoffset="): loadedSpectrum['Metadata']['Low Energy'] = float(
                        line.replace("widthoffset=", "").rstrip('\n').replace(",", "."))
                    if line.startswith("width="): numEnergyPoints = int(
                        line.replace("width=", "").rstrip('\n').replace(",", "."))
                    if line.startswith("widthdelta="): loadedSpectrum['Metadata']['Energy Step'] = float(
                        line.replace("widthdelta=", "").rstrip('\n').replace(",", "."))

                    # -------------------

                    # ---- Analyzer slit axis ----
                    if line.startswith("heightoffset="): loadedSpectrum['Metadata']['Low Analyzer Angle'] = float(
                        line.replace("heightoffset=", "").rstrip('\n').replace(",", "."))
                    if line.startswith("height="): numAnalyzerAnglePoints = int(
                        line.replace("height=", "").rstrip('\n').replace(",", "."))
                    if line.startswith("heightdelta="): loadedSpectrum['Metadata']['Analyzer Angle Step'] = float(
                        line.replace("heightdelta=", "").rstrip('\n').replace(",", "."))
                    # -------------------

                    # ---- Deflector axis ----
                    if line.startswith("depthoffset="): loadedSpectrum['Metadata']['Low Deflector Angle'] = float(
                        line.replace("depthoffset=", "").rstrip('\n').replace(",", "."))
                    if line.startswith("depth="): numDeflectorAnglePoints = int(
                        line.replace("depth=", "").rstrip('\n').replace(",", "."))
                    if line.startswith("depthdelta="): loadedSpectrum['Metadata']['Deflector Angle Step'] = float(
                        line.replace("depthdelta=", "").rstrip('\n').replace(",", "."))
                # -------------------

            filename = regionNames[regionIndex - 1] + '.ini'
            with z.open(filename) as f:
                for lineNumber, l in enumerate(f):
                    line = l.decode()
                    if line.startswith("Region Name="): loadedSpectrum['Metadata']['Region Name'] = line.replace(
                        "Region Name=", "").rstrip('\r\n')
                    if line.startswith("Lens Mode="): loadedSpectrum['Metadata']['Lens Mode'] = line.replace(
                        "Lens Mode=", "").rstrip('\r\n')
                    if line.startswith("Pass Energy="): loadedSpectrum['Metadata']['Pass Energy'] = int(
                        line.replace("Pass Energy=", "").rstrip('\r\n'))
                    if line.startswith("Number of Sweeps="): loadedSpectrum['Metadata']['Number of Sweeps'] = int(
                        line.replace("Number of Sweeps=", "").rstrip('\r\n'))
                    if line.startswith("Excitation Energy="): loadedSpectrum['Metadata']['Excitation Energy'] = float(
                        line.replace("Excitation Energy=", "").rstrip('\r\n').replace(",", "."))
                    if line.startswith("Energy Scale="): loadedSpectrum['Metadata']['Energy Scale'] = line.replace(
                        "Energy Scale=", "").rstrip('\r\n')
                    if line.startswith("Acquisition Mode="): loadedSpectrum['Metadata'][
                        'Acquisition Mode'] = line.replace("Acquisition Mode=", "").rstrip('\r\n')
                    if line.startswith("Energy Unit="): loadedSpectrum['Metadata']['Energy Unit'] = line.replace(
                        "Energy Unit=", "").rstrip('\r\n')
                    if line.startswith("Step Time="): loadedSpectrum['Metadata']['Step Time'] = float(
                        line.replace("Step Time=", "").rstrip('\r\n').replace(",", "."))
                    if line.startswith("Detector First X-Channel="): loadedSpectrum['Metadata'][
                        'Detector First X-Channel'] = int(line.replace("Detector First X-Channel=", "").rstrip('\r\n'))
                    if line.startswith("Detector Last X-Channel="): loadedSpectrum['Metadata'][
                        'Detector Last X-Channel'] = int(line.replace("Detector Last X-Channel=", "").rstrip('\r\n'))
                    if line.startswith("Detector First Y-Channel="): loadedSpectrum['Metadata'][
                        'Detector First Y-Channel'] = int(line.replace("Detector First Y-Channel=", "").rstrip('\r\n'))
                    if line.startswith("Detector Last Y-Channel="): loadedSpectrum['Metadata'][
                        'Detector Last Y-Channel'] = int(line.replace("Detector Last Y-Channel=", "").rstrip('\r\n'))
                    if line.startswith("Number of Slices="): loadedSpectrum['Metadata']['Number of Slices'] = int(
                        line.replace("Number of Slices=", "").rstrip('\r\n'))
                    if line.startswith("File="):
                        path = line.replace("File=", "").rstrip('\n').replace("\\\\", "\\")
                        loadedSpectrum['Metadata']['File Path'] = path[:path.rindex('\\') + 1] + fileName
                    if line.startswith("Sequence="): loadedSpectrum['Metadata']['Sequence'] = line.replace("Sequence=",
                                                                                                           "").rstrip(
                        '\r\n').replace("\\\\", "\\")
                    if line.startswith("Spectrum Name="): loadedSpectrum['Metadata']['Spectrum Name'] = line.replace(
                        "Spectrum Name=", "").rstrip('\r\n')
                    if line.startswith("Instrument="): loadedSpectrum['Metadata']['Instrument'] = line.replace(
                        "Instrument=", "").rstrip('\r\n')
                    if line.startswith("Location="): loadedSpectrum['Metadata']['Location'] = line.replace("Location=",
                                                                                                           "").rstrip(
                        '\r\n')
                    if line.startswith("User="): loadedSpectrum['Metadata']['User'] = line.replace("User=", "").rstrip(
                        '\r\n')
                    if line.startswith("Sample="): loadedSpectrum['Metadata']['Sample'] = line.replace("Sample=",
                                                                                                       "").rstrip(
                        '\r\n')
                    if line.startswith("Comments="): loadedSpectrum['Metadata']['Comments'] = line.replace("Comments=",
                                                                                                           "").rstrip(
                        '\r\n')
                    if line.startswith("Date="): loadedSpectrum['Metadata']['Date'] = line.replace("Date=", "").rstrip(
                        '\r\n')
                    if line.startswith("Time="): loadedSpectrum['Metadata']['Time'] = line.replace("Time=", "").rstrip(
                        '\r\n')
                    if line.startswith("Time per Spectrum Channel="): loadedSpectrum['Metadata'][
                        'Time per Spectrum Channel'] = float(
                        line.replace("Time per Spectrum Channel=", "").rstrip('\r\n').replace(",", "."))
                    if line.startswith("DetectorMode="): loadedSpectrum['Metadata']['DetectorMode'] = line.replace(
                        "DetectorMode=", "").rstrip('\r\n')
                    if line.startswith("A="): loadedSpectrum['Metadata']['Manipulator Azimuth'] = float(
                        line.replace("A=", "").rstrip('\r\n').replace(",", "."))
                    if line.startswith("P="): loadedSpectrum['Metadata']['Manipulator Polar'] = float(
                        line.replace("P=", "").rstrip('\r\n').replace(",", "."))
                    if line.startswith("T="): loadedSpectrum['Metadata']['Manipulator Tilt'] = float(
                        line.replace("T=", "").rstrip('\r\n').replace(",", "."))
                    if line.startswith("X="): loadedSpectrum['Metadata']['Manipulator X'] = float(
                        line.replace("X=", "").rstrip('\r\n').replace(",", "."))
                    if line.startswith("Y="): loadedSpectrum['Metadata']['Manipulator Y'] = float(
                        line.replace("Y=", "").rstrip('\r\n').replace(",", "."))
                    if line.startswith("Z="): loadedSpectrum['Metadata']['Manipulator Z'] = float(
                        line.replace("Z=", "").rstrip('\r\n').replace(",", "."))

            angleStart = loadedSpectrum['Metadata']['Low Analyzer Angle']
            angleStep = loadedSpectrum['Metadata']['Analyzer Angle Step']
            angleEnd = loadedSpectrum['Metadata']['Low Analyzer Angle'] + (numAnalyzerAnglePoints - 1) * angleStep

            energyStart = loadedSpectrum['Metadata']['Low Energy']
            energyStep = loadedSpectrum['Metadata']['Energy Step']
            energyEnd = loadedSpectrum['Metadata']['Low Energy'] + (numEnergyPoints - 1) * energyStep
            # loadedSpectrum['yAxis']=np.linspace(energyStart,energyEnd,numEnergyPoints)
            # if loadedSpectrum['Metadata']['Energy Scale'].startswith("Kinetic"):loadedSpectrum['yAxisLabel']="Kinetic energy (eV)"
            # else: loadedSpectrum['yAxisLabel']="Binding energy (eV)"
            # loadedSpectrum['yAxisLabel']="Energy (eV)"

            loadedSpectrum['Axis'][0] = np.linspace(energyStart, energyEnd, numEnergyPoints)
            if loadedSpectrum['Metadata']['Energy Scale'].startswith("Kinetic"):
                loadedSpectrum['AxisLabel'][0] = "Kinetic energy"
            else:
                loadedSpectrum['AxisLabel'][0] = "Binding energy"
            loadedSpectrum['AxisUnits'][0] = "eV"

            loadedSpectrum['Axis'][1] = np.linspace(angleStart, angleEnd, numAnalyzerAnglePoints)
            loadedSpectrum['AxisLabel'][1] = "Analyzer angle"
            loadedSpectrum['AxisUnits'][1] = "$\degree$"

            angleStart = loadedSpectrum['Metadata']['Low Deflector Angle']
            angleStep = loadedSpectrum['Metadata']['Deflector Angle Step']
            angleEnd = loadedSpectrum['Metadata']['Low Deflector Angle'] + (numDeflectorAnglePoints - 1) * angleStep
            loadedSpectrum['Axis'][2] = np.linspace(angleStart, angleEnd, num=numDeflectorAnglePoints, endpoint=True)
            loadedSpectrum['AxisLabel'][2] = "Deflector angle"
            loadedSpectrum['AxisUnits'][2] = "$\degree$"

            # ---------- Load the data matrix ----------

            filename = 'Spectrum_' + regionNames[regionIndex - 1] + '.bin'
            #tempDirectory = tempfile.TemporaryDirectory()
            #z.extract(filename, tempDirectory.name)


            # data is loaded in (analyzer angle, energy, deflector angle)
            with z.open(filename, "r") as f:
                data = f.read()
                binaryBlob = np.frombuffer(data, dtype=np.float32)
            # binaryBlob has energy x def angle x ana angle

            x = loadedSpectrum['Axis'][1]  # analyzer angle
            y = loadedSpectrum['Axis'][0]  # energy
            z = loadedSpectrum['Axis'][2]  # deflector angle

            loadedSpectrum['data'] = np.zeros((len(x), len(y), len(z)), np.float32)

            for jj in range(len(x)):
                precalc1 = jj * len(y)
                for kk in range(len(z)):
                    index = (kk * len(x) * len(y)) + precalc1
                    loadedSpectrum['data'][jj, :, kk] = binaryBlob[index:index + len(y)]
        loadedSpectrum['data'] = loadedSpectrum['data'].transpose(1, 0, 2)
        if beQuiet == False:
            if len(z) == 1:
                print("Single frame acquired at {:.2f} deg".format(z[0]))
            else:
                print("Deflected from {:.2f} deg to {:.2f} deg with stepsize {:.2f}".format(z[0], z[-1],
                                                                                            abs(z[1] - z[0])))
        return loadedSpectrum

    if fileName.endswith(".sp2"):
        loadedSpectrum = load_sp2(fileName, beQuiet, regionIndex)
    if fileName.endswith(".txt"):
        loadedSpectrum = load_txt(fileName, beQuiet, regionIndex, whichManipulatorAxis)
    if fileName.endswith(".ibw"):
        loadedSpectrum = load_ibw(fileName, beQuiet, regionIndex, whichManipulatorAxis)
    if fileName.endswith(".zip"):
        loadedSpectrum = load_zip(fileName, beQuiet, regionIndex)
    if fileName.endswith(".itx"):
        loadedSpectrum = load_itx(fileName)

    if beQuiet == False:
        printMetaData(spectrum=loadedSpectrum)

    return loadedSpectrum
