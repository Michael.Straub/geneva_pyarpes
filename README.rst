Mirror of PyARPES from the Lanzara Group (https://github.com/chstan/arpes) including configurations useful for the Baumberger group (geneva)

The local Geneva version is currently developed by Michael Straub of the Baumberger group at the University of Geneva

Installation
============

This mirror has to be installed from source at this point. The easiest is to follow the following instructions:

1. Install Miniconda (minimal installer) or Anaconda (includes all common scientific python packages) according to instructions_.

.. _instructions: https://docs.conda.io/en/latest/miniconda.html

2. Clone or otherwise download the repository ::

	git clone https://gitlab.unige.ch/Michael.Straub/geneva_pyarpes.git

3. Make a conda environment::

	cd path/to/local_repository
	conda env create -f environment.yml

4. Activate the environment::

	conda activate arpes

5. If you want to change things in your local source code install it in editable mode::

	pip install -e

Recommended:

6. Install JupyterLab_ (e.g. with conda).

.. _JupyterLab: https://jupyterlab.readthedocs.io/en/stable/getting_started/installation.html

7. Using JupyerLab you can choose the environment in the top right.

Documentation
=============

The documentation of the original PyARPES can be found here_.

.. _here: https://arpes.readthedocs.io/en/latest/

The arpes branch currently has no documentation available.


PyARPES
=======

PyARPES simplifies the analysis and collection of angle-resolved photoemission spectroscopy (ARPES) and emphasizes

* modern, best practices for data science
* support for a standard library of ARPES analysis tools mirroring those available in Igor Pro
* interactive and extensible analysis tools

It supports a variety of data formats from synchrotron and laser-ARPES sources including ARPES at the Advanced
Light Source (ALS), the data produced by Scienta Omicron GmbH's "SES Wrapper", data and experiment files from
Igor Pro, NeXuS files, and others.

PyARPES is currently developed by Conrad Stansbury of the Lanzara Group at the University of California, Berkeley.

Citing PyARPES
--------------

If you use PyARPES in your work, please support the development of scientific software by acknowledging its usefulness to you with a citation. The simplest way to do this is to cite the paper describing the package in SoftwareX


    @article{
        stansburypyarpes,
        title = {PyARPES: An analysis framework for multimodal angle-resolved photoemission spectroscopies},
        journal = {SoftwareX},
        volume = {11},
        pages = {100472},
        year = {2020},
        issn = {2352-7110},
        doi = {https://doi.org/10.1016/j.softx.2020.100472},
        url = {https://www.sciencedirect.com/science/article/pii/S2352711019301633},
        author = {Conrad Stansbury and Alessandra Lanzara},
        keywords = {ARPES, NanoARPES, Pump-probe ARPES, Photoemission, Python, Qt, Jupyter},
        abstract = {},
    }

